// Import Packages
import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {

	// State hooks for form.
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    // State hook for the button.
    const [isActive, setIsActive] = useState(false);

    // To use the navigate
    const navigate = useNavigate();

    const { user, setUser } = useContext(UserContext)
    
    // Validation
    const regularExpression = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/;
    const listOfEmail = ["@gmail.com", "@yahoo.com", "@hotmail.com"];

    // Function Start
    function registerUser(e) {
    	// Prevents page redirection via form submission
    	e.preventDefault();

    	// Fetching the register in API
    	fetch (`${process.env.REACT_APP_API_URL}/users/register`, {
    		method: "POST",
    		headers: {
    			"Content-Type": "application/json"
    		},
    		body: JSON.stringify({
    			firstName: firstName,
    			lastName: lastName,
    			email: email,
    			password: password1
    		})
	    }).then(response => response.json()).then(data => { 
	    	if (data === true) {
	    		Swal.fire({
                    icon: 'success',
                    title: 'Registration successful!',
                    timer: 2000
                })

                navigate("/login"); 

	    	} else if (data === false) {
	    		Swal.fire({
                    icon: 'error',
                    title: 'Duplicate email found',
                    text: 'Please provide a different email.'
                })
	    	}
	    });

    	// Clear input fields
    	setFirstName(''); setLastName(''); setEmail(''); setPassword1(''); setPassword2('');
    }
    // Function End

    // useEffect to enable the button when there are no empty fields.
    useEffect(() => {
    	if((firstName !== "" && 
    		lastName !== "" && 
    		email !== "" && 
    		password1 !== "" && 
    		password2 !== "") && 
    		(password1 === password2))
    		{
    		setIsActive(true);
    	} else {
    		setIsActive(false);
    	}
	}, [firstName, lastName, email, password1, password2]);

    // Function Start
    var checkPassword1 = function () {
    	if(!regularExpression.test(password1)) {
    		document.getElementById('passMessage1').innerHTML = "Minimum length of 8 and containing at least 1 upper, 1 lower, 1 numeric, and 1 special character. (a-z, A-Z, 0-9, #$%@)";
    	} else {
    		document.getElementById('passMessage1').innerHTML = "";
    	}
    }

    var checkPassword2 = function () {
    	if(password1 === password2) {
    		document.getElementById('passMessage2').innerHTML = "Passwords matched!";
    	} else {
    		document.getElementById('passMessage2').innerHTML = "Password do not match!"
    	}
    }

    var checkEmail = function () {
    	for (let index = 0; index < listOfEmail.length; index++) {
	    	if (email.includes(listOfEmail[index])) {
	    		document.getElementById('emailMessage').innerHTML = "Valid email!";
	    		break;
	    	} else {
	    		document.getElementById('emailMessage').innerHTML = "Please use a proper a proper email!";
	    	}
   		}
    }
    // Function End

	return(
		(user.id) ?
			<Navigate to="/" />
		:
			<>
			<h2>Register</h2>
			<hr />
			<Form onSubmit={(e) => registerUser(e)}>
				{/* First Name */}
				<Form.Group controlId="userFirstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter first name"
						value={ firstName }
						onChange={e => setFirstName(e.target.value)}
						required
					 />
				</Form.Group>

				{/* Last Name */}
				<Form.Group controlId="userLastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter last name"
						value={ lastName }
						onChange={e => setLastName(e.target.value)}
						required
					 />
				</Form.Group>

				{/* Email */}
				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter an email"
						value={ email }
						onChange={e => setEmail(e.target.value)}
						onKeyUp = {e => checkEmail(e.target.value)}
						required
					 />
					 <Form.Text id="emailMessage" className="registerMessage text-muted">We'll never share your email with anyone.</Form.Text>
				</Form.Group>

				{/* Password */}
				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Password"
						value={ password1 }
						onChange={e => setPassword1(e.target.value)}
						onKeyUp = {e => checkPassword1(e.target.value)}
						required
					 />
					 <Form.Text id="passMessage1" className="registerMessage text-muted"></Form.Text>
				</Form.Group>

				{/* Confirm Password */}
				<Form.Group controlId="password2">
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify Password"
						value={ password2 }
						onChange={e => setPassword2(e.target.value)}
						onKeyUp = {e => checkPassword2(e.target.value)}
						required
					 />
					 <Form.Text id="passMessage2" className="registerMessage text-muted">Please make sure your passwords match.</Form.Text>
				</Form.Group>

				<Form.Group controlId="registeredUser">
	            	<Form.Text>Already have an account? <a href="./login">Click here</a> to login.</Form.Text>
	            </Form.Group>
				{/*Conditional Rending for Submit Button*/}
				{
					isActive ?
						<Button variant="primary" type="submit" id="submitBtn" className="mt-2" to="/login">Register</Button>
						:
						<Button variant="dark" type="submit" id="submitBtn" className="mt-2" disabled>Register</Button>
				}
			</Form>
			</>

	)

}