// Imported Packages
import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../../UserContext';
import Swal from 'sweetalert2';
import Table from 'react-bootstrap/Table'

// Imported Components
import ProductTable from '../../components/ProductTable';

// Import Admin Components

// JS File Default Function
export default function AdminDashboard() {
	const { user, setUser } = useContext(UserContext)

	// variable used for mapping
	const [ products, setProducts ] = useState([])
	
	// useEffect to fetch the products and map it to the dashboard
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/viewAll`, { 
				headers: {
	        		authorization: `Bearer ${ localStorage.getItem('token') }`
	        	}
	    }).then(response => response.json()).then(data => {
	    	setProducts(data.map(product => {
	    		return (
	    			<ProductTable key={ product._id } product = { product } />
	    		)
	    	}))
	    }).catch(error => error);
	})
	
	// Start Function DELETE ALL
	function deleteAll(e) {
		// Prevents page redirection via form submission
    	e.preventDefault();

    	// Pop up message
		Swal.fire({
		  title: 'Are you sure?',
		  text: "All products will be deleted if you press 'OK'",
		  icon: 'warning',
		  showCancelButton: true
		}).then((result) => {
		  if (result.isConfirmed) {
		  	// Update Product API
		  	fetch(`https://e-commerce-9otx.onrender.com/products/delete`, {
	    		method: "DELETE",
	    		headers: {
	    			Authorization: `Bearer ${ localStorage.getItem('token') }`,
	    			"Content-Type": "application/json"
	    		}
	    	}).then(response => response.json()).then(data => data);
		    Swal.fire({
		      title: 'Deleted!',
		      text: 'All products are now deleted!',
		      icon: 'success',
		      timer: 2000
		    })
		  }
		})
	}

	return (
		(user.isAdmin) ?
		<>	
			<h2>Admin User Dashboard</h2>
			<Link to='/admin/product/create'><Button variant="secondary" className="me-2">Create New Product</Button></Link>
			<Button variant="danger" onClick={ deleteAll }>DELETE ALL</Button>
			<hr />

			<h3>Table of Product Registered</h3>
			<div className="table-responsive-sm">
			  <table className="table" Responsive>
				<thead>
			    	<tr>
					    <th width="170">Id</th>
					    <th width="170">Name</th>
					    <th width="400">Description</th>
					    <th width="85">Price</th>
					    <th width="85">isActive</th>
					    <th width="170">createdOn</th>
					    <th width="80">Actions</th>
					</tr>
			 	</thead>

			 	<tbody>
					{ products }
				</tbody>
			  </table>
		  </div>
		</>
		:
		<Navigate to="/" />
	)
}