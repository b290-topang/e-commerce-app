// Imported packages
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Home() {

    const data = {
        title: "Welcome to Wilbo Market Place",
        content: "Home page shows the 3 current hot items of the website",
        destination: "/",
        label: "Click here"
    }
    return (
        <>  
            <Banner data={ data }/>
            <Highlights />
        </>
    )
}
