// Imported Packages
import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Card, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Table from 'react-bootstrap/Table'

// Imported Components
import ProductCard from '../components/ProductCard';

export default function Products() {
	const { user, setUser } = useContext(UserContext)

	const [ products, setProducts ] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/viewAllActive`)
		.then(response => response.json()).then(data => {
	    	setProducts(data.map(product => {
	    		return (
	    			<ProductCard key={ product._id } product = { product } />
	    		)
	    	}))
	    }).catch(error => error);
	})

	return (
		<>	
		<h2>List of Products</h2>
		<hr />

		<Container fluid>
        <Row className="mt-3 mb-3">
			{ products }
		</Row>
        </Container>
		</>
	)
}
