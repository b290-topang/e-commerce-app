// List of Imports
import { useState } from 'react';
import { useContext } from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import UserContext	from '../UserContext';

// AppNavBar Function
export default function AppNavbar() {
	const { user } = useContext(UserContext);
	
	return (
		<Navbar bg="light" expand="lg" className="sticky-top">
			<Container fluid>
				<Navbar.Brand as={ NavLink } to="/">WMP</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto">
						<Nav.Link as={ NavLink } to="/">Home</Nav.Link>

						{/* Conditional Statement*/}
						{
							(user.id) ?
								(user.isAdmin) ?
								<>
									<Nav.Link as={ NavLink } to="/products">Products</Nav.Link>
									<Nav.Link as={ NavLink } to="/admin/dashboard">Dashboard</Nav.Link>
									<Nav.Link as={ NavLink } to="/logout">Logout</Nav.Link>
								</>
								:
								<>	
									<Nav.Link as={ NavLink } to="/products">Products</Nav.Link>
									<Nav.Link as={ NavLink } to="/myprofile">My Profile</Nav.Link>
									<Nav.Link as={ NavLink } to="/logout">Logout</Nav.Link>
								</>
							:
							<>
								<Nav.Link as={ NavLink } to="/login">Login</Nav.Link>
								<Nav.Link as={ NavLink } to="/register">Register</Nav.Link>
							</>
						}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}